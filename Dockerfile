FROM alpine:3.21@sha256:a8560b36e8b8210634f77d9f7f9efd7ffa463e380b75e2e74aff4511df3ef88c

COPY files/ /

ENV BACKUP_CRON_EXPRESSION="" \
    BACKUP_CRON_LOGLEVEL="6" \
    BACKUP_SUCCESS_WEBHOOK="" \
    BACKUP_RESTIC_FORGET_KEEP_ARGS="" \
    BACKUP_MYSQL_HOST="" \
    BACKUP_MYSQL_USER="" \
    BACKUP_MYSQL_PASS="" \
    BACKUP_MYSQL_VERIFY_SSL="yes" \
    BACKUP_UNLOCK_ON_START="yes" \
    RESTIC_REPOSITORY="" \
    RESTIC_PASSWORD="" \
    ENVOY_SIDECAR_SUPPORT="no" \
    ENVOY_SIDECAR_WAIT_TIMEOUT="60"

# renovate: datasource=repology depName=alpine_3_21/restic depType=dependencies versioning=loose
ARG RESTIC_VERSION="0.17.3-r2"

RUN apk add --no-cache \
        bash~=5 \
        run-parts~=4 \
        restic="${RESTIC_VERSION}" \
        moreutils~=0 \
        mariadb-client~=11 \
        mariadb-connector-c-dev~=3 \
        curl~=8

ENTRYPOINT ["/usr/local/sbin/docker-entrypoint"]
CMD ["/usr/local/sbin/docker-cmd"]
